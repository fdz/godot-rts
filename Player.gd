extends Spatial

const ZOOM_SPEED = 3

export (float, 0.1, 1000) var camera_distance:float = 15
onready var cur_unit = get_node("../Unit")
onready var _grid = get_node("../Environment/Grid")
onready var _camera = $Camera

const _ORIENTATION_STEP = deg2rad(45)
var _orientation:Transform

onready var _placeable = preload("res://FDZGridPlaceable.tscn")

func _ready() -> void:
	_orientation = transform.basis
	self.camera_distance = camera_distance

func _physics_process(delta: float) -> void:
	if cur_unit == null:
		return
	global_transform.origin = global_transform.origin.linear_interpolate(cur_unit.global_transform.origin, delta * 4)

func _process(delta: float) -> void:
	if Input.is_action_pressed("zoom_in"):
		camera_distance -= delta * ZOOM_SPEED
	if Input.is_action_pressed("zoom_out"):
		camera_distance += delta * ZOOM_SPEED
	
	camera_distance = clamp(camera_distance, 0.1, camera_distance)
	_camera.translation = Vector3(camera_distance, camera_distance * 2, camera_distance)

	if Input.is_action_just_pressed("camera_rotate_right"):
		_orientation = _orientation.rotated(Vector3.UP, _ORIENTATION_STEP)
	if Input.is_action_just_pressed("camera_rotate_left"):
		_orientation = _orientation.rotated(Vector3.UP, -_ORIENTATION_STEP)

	var q_from = Quat(transform.basis)
	var q_to = Quat(_orientation.basis)
	transform.basis = Basis(q_from.slerp(q_to, delta * 8))
	
	if Input.is_action_just_pressed("click_right"):
		var mouse_position = get_viewport().get_mouse_position()
		var from = _camera.project_ray_origin(mouse_position)
		var to = from + _camera.project_ray_normal(mouse_position) * 10000
		var hit = get_world().direct_space_state.intersect_ray(from, to, [], Phys.DEFAULT || Phys.GRIDS, true, true)
		if hit.size() > 0 and hit.collider is Unit:
			cur_unit = hit.collider
	
	if Input.is_action_just_pressed("click_left") and cur_unit != null:
		var mouse_position = get_viewport().get_mouse_position()
		var from = _camera.project_ray_origin(mouse_position)
		var to = from + _camera.project_ray_normal(mouse_position) * 10000
		var hit = get_world().direct_space_state.intersect_ray(from, to, [], Phys.GRIDS, true, true)
		if hit.size() > 0:
			cur_unit.move_to(hit.position)
	
	if Input.is_action_just_pressed("place"):
		var mouse_position = get_viewport().get_mouse_position()
		var from = _camera.project_ray_origin(mouse_position)
		var to = from + _camera.project_ray_normal(mouse_position) * 10000
		var hit = get_world().direct_space_state.intersect_ray(from, to, [], Phys.GRIDS, true, true)
		if hit.size() > 0:
			var p =_placeable.instance()
			owner.add_child(p)
			if p.place_from_global_point(_grid, hit.position):
				_grid.pathfiding_update()
				print("placed " + str(p.name))
			else:
				p.free()
	
	if Input.is_action_just_pressed("delete"):
		var mouse_position = get_viewport().get_mouse_position()
		var from = _camera.project_ray_origin(mouse_position)
		var to = from + _camera.project_ray_normal(mouse_position) * 10000
		var hit = get_world().direct_space_state.intersect_ray(from, to, [], Phys.DEFAULT, true, true)
		if hit.size() > 0 and hit.collider is FDZGridPlaceable:
			hit.collider.unplace()
			_grid.pathfiding_update()