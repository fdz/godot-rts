class_name UnitAction

var _name:String
var _unit:Unit
var _started:bool
var _cancel:bool
var _radius:float
var _target

func _init(name:String, unit:Unit, radius:float, target) -> void:
	_name = name
	_unit = unit
	_radius = radius
	_target = target
	assert(target is Spatial || target is Vector3)

func get_target() -> Vector3:
	if _target is Spatial:
		return _target.global_transform.origin
	return _target

func in_range() -> bool:
	return _unit.global_transform.origin.distance_to(get_target()) <= _radius

func can_queue() -> bool:
	return true

func start() -> void:
	pass

func update(delta:float) -> void:
	pass

func cancel() -> void:
	_cancel = true

func is_done() -> bool:
	return false