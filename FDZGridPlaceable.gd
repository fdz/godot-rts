extends Area
class_name FDZGridPlaceable

const PTYPE_WALL = 1 

var PLACEABLE_TYPE = PTYPE_WALL

var grid:Object = null
var cell_id:int
var neighbour_cell_id:int
var edge:Vector3

func unplace() -> void:
	if grid != null:
		grid.placeables_erase(self)
		grid.cells_erase_placeable(cell_id, self)
		grid.cells_erase_placeable(neighbour_cell_id, self)
	queue_free()

func place_from_global_point(grid:Object, global_point:Vector3) -> bool:
	if not grid.has_global_point(global_point):
		return false
	
	var cell_id = grid.get_id_from_global_point(global_point)
	var cell_position = grid.get_cell_global_point(cell_id)

	var edge = grid.get_cell_edge(cell_position, global_point)
	if edge == Vector3.ZERO:
		return false
	
	if edge == Vector3.LEFT:
		neighbour_cell_id = cell_id - 1
	elif edge == Vector3.RIGHT:
		neighbour_cell_id = cell_id + 1
	elif edge == Vector3.BACK:
		neighbour_cell_id = cell_id + grid.GRAPH_SIZE
	elif edge == Vector3.FORWARD:
		neighbour_cell_id = cell_id - grid.GRAPH_SIZE
	
	for p in grid.cell_get_placeables(cell_id):
		if p.PLACEABLE_TYPE == PTYPE_WALL and p.neighbour_cell_id == neighbour_cell_id:
			return false
	
	for p in grid.cell_get_placeables(neighbour_cell_id):
		if p.PLACEABLE_TYPE == PTYPE_WALL and p.neighbour_cell_id == cell_id:
			return false
	
	grid.cell_push_placeable(cell_id, self)
	grid.cell_push_placeable(neighbour_cell_id, self)
	
	# SET GLOBAL TRANSFORM
	global_transform.origin = cell_position + edge * grid.CELL_SIZE_HALF
	if edge == Vector3.RIGHT or edge == Vector3.LEFT:
		global_transform.basis = Basis(Vector3.UP, PI / 2)
	else:
		global_transform.basis = Basis(Vector3.UP, 0)
	
	self.grid = grid
	self.edge = edge
	self.cell_id = cell_id
	grid.placeables_push(self)
	
	return true

func pathfiding_update() -> void:
	if edge == Vector3.LEFT:
		var next = cell_id - 1
		grid.pathfinding_disconnect_points(cell_id, next)
		grid.pathfinding_disconnect_points(cell_id, next - grid.GRAPH_SIZE)
		grid.pathfinding_disconnect_points(cell_id, next + grid.GRAPH_SIZE)
		grid.pathfinding_disconnect_points(next, cell_id - grid.GRAPH_SIZE)
		grid.pathfinding_disconnect_points(next, cell_id + grid.GRAPH_SIZE)
	elif edge == Vector3.RIGHT:
		var next = cell_id + 1
		grid.pathfinding_disconnect_points(cell_id, next)
		grid.pathfinding_disconnect_points(cell_id, next - grid.GRAPH_SIZE)
		grid.pathfinding_disconnect_points(cell_id, next + grid.GRAPH_SIZE)
		grid.pathfinding_disconnect_points(next, cell_id - grid.GRAPH_SIZE)
		grid.pathfinding_disconnect_points(next, cell_id + grid.GRAPH_SIZE)
	elif edge == Vector3.BACK:
		var next = cell_id + grid.GRAPH_SIZE
		grid.pathfinding_disconnect_points(cell_id, next)
		grid.pathfinding_disconnect_points(cell_id, next - 1)
		grid.pathfinding_disconnect_points(cell_id, next + 1)
		grid.pathfinding_disconnect_points(next, cell_id - 1)
		grid.pathfinding_disconnect_points(next, cell_id + 1)
	elif edge == Vector3.FORWARD:
		var next = cell_id - grid.GRAPH_SIZE
		grid.pathfinding_disconnect_points(cell_id, next)
		grid.pathfinding_disconnect_points(cell_id, next - 1)
		grid.pathfinding_disconnect_points(cell_id, next + 1)
		grid.pathfinding_disconnect_points(next, cell_id - 1)
		grid.pathfinding_disconnect_points(next, cell_id + 1)