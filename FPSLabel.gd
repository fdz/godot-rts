extends Label

func _process(delta: float) -> void:
	if false:
		delta = 0
	text = str(Engine.get_frames_per_second()) + "\n"
	if OS.vsync_enabled:
		text += "VSync ON" + "\n"
	else:
		text += "VSync OFF" + "\n"
	text += "time_scale: " + str(Engine.time_scale) + "\n"