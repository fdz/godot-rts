tool
extends Spatial
class_name FDZGrid

class FDZGridCell:
	var ocuppiers_count:int
	var placeables:Array = []

export var CONNECT_DIAGONALLY:bool = true
export var CELL_SIZE:float = 3.0
export var GRAPH_SIZE:int = 50

var CELL_SIZE_HALF:float
var CELL_COUNT:int
var GRAPH_SIZE_HALF:int

var BOUNDS:Rect2

const IM_OFFSET = Vector3.UP * 0.05

var _generation_finished:bool
var _gdfuncst
var _graph:AStar = AStar.new()
var _point_count:int

var _cells:Array = []
var _placeables:Array = []

onready var _im = get_node("Draw")

var _timer:float
var _dirty:bool = true

func _init_values() -> void:
	assert(GRAPH_SIZE % 2 == 0)
	CELL_SIZE_HALF = CELL_SIZE / 2
	CELL_COUNT = GRAPH_SIZE * GRAPH_SIZE
	GRAPH_SIZE_HALF = int(round(float(GRAPH_SIZE) / 2))
	BOUNDS = Rect2(-GRAPH_SIZE_HALF * CELL_SIZE, -GRAPH_SIZE_HALF * CELL_SIZE, GRAPH_SIZE * CELL_SIZE, GRAPH_SIZE * CELL_SIZE)

func _ready() -> void:
	_init_values()
	if Engine.editor_hint:
		return
	_gdfuncst = _graph_generation()

func _process(delta: float) -> void:
	if Engine.editor_hint:
		_init_values()
		_timer -= delta
		if _timer > 0:
			return
		_timer = 0.75
		_draw_grid()
		return
	if _gdfuncst is GDScriptFunctionState and _gdfuncst.is_valid():
		_gdfuncst = _gdfuncst.resume()
	if _generation_finished:
		if _dirty:
			_dirty = false
			_update_im()

func placeables_push(placeable:Object) -> void:
	_placeables.push_back(placeable)

func placeables_erase(placeable:Object) -> void:
	_placeables.erase(placeable)

func cell_get_placeables(cell_id:int) -> Array:
	return _cells[cell_id].placeables

func cell_push_placeable(cell_id:int, placeable:Object) -> void:
	_cells[cell_id].placeables.push_back(placeable)

func cell_erase_placeable(cell_id:int, placeable:Object) -> void:
	_cells[cell_id].placeables.erase(placeable)

func set_cell_occupied_from_global_point(global_point:Vector3, occupied:bool) -> void:
	var id = get_id_from_global_point(global_point)
	var cell:FDZGridCell = _cells[id]
	if occupied:
		if cell.ocuppiers_count == 0:
			_graph.set_point_disabled(id, true)
			_dirty = true
		cell.ocuppiers_count += 1
	else:
		if cell.ocuppiers_count == 1:
			_graph.set_point_disabled(id, false)
			_dirty = true
		if cell.ocuppiers_count > 0:
			cell.ocuppiers_count -= 1

func has_global_point(global_point:Vector3) -> bool:
	return has_local_point(to_local(global_point))

func has_local_point(local_point:Vector3) -> bool:
	return BOUNDS.has_point(Vector2(local_point.x, local_point.z))

func is_id_valid(id:int) -> bool:
	return id >= 0 and id < CELL_COUNT

func get_id_from_global_point(global_point:Vector3) -> int:
	return get_id_from_local_point(to_local(global_point))

func get_id_from_local_point(position:Vector3) -> int:
	var z = ceil(position.z / CELL_SIZE + GRAPH_SIZE_HALF) * GRAPH_SIZE
	var x = ceil(position.x / CELL_SIZE + GRAPH_SIZE_HALF)
	return int(z + x - GRAPH_SIZE - 1)

func get_cell_global_point(id:int) -> Vector3:
	return global_transform.origin + _graph.get_point_position(id)

func get_global_point_path(from:Vector3, to:Vector3, radius:float = 0) -> PoolVector3Array:
	from = to_local(from)
	to = to_local(to)
	var id_from = _graph.get_closest_point(from)
	var id_to = _graph.get_closest_point(to)
	# RETURN EMTPY PATH IF THE IDS ARE THE SAME
	if id_from == id_to:
		return PoolVector3Array()
	# IF THE TARGET POSITION IS DISABLED GET THE NEAREST AVAILABLE POSITION
	if _graph.is_point_disabled(id_to):
		if radius > 0:
			if radius < CELL_SIZE:
				radius = CELL_SIZE
			var radius_in_tiles = ceil(radius / CELL_SIZE)
			var center = _graph.get_point_position(id_to)
			var center_id = id_to
			var distance = INF
			for x in range(-radius_in_tiles, radius_in_tiles + 1):
				for y in range(-radius_in_tiles, radius_in_tiles + 1):
					var aux_id = get_id_from_local_point(center + Vector3(x * CELL_SIZE, 0, y * CELL_SIZE))
					if aux_id == center_id or _graph.is_point_disabled(aux_id):
						continue
					var aux_distance = _graph.get_point_position(aux_id).distance_to(to)
					if aux_distance < distance or distance == INF:
						id_to = aux_id
						distance = aux_distance
		else:
			return PoolVector3Array()
	# GET PATH AND ADD GLOBAL_TRANSFORM.ORIGIN
	var path = _graph.get_point_path(id_from, id_to)
	for i in path.size():
		path.set(i, global_transform.origin + path[i])
	return path

func get_cell_edge(cell_globlal_point:Vector3, global_point:Vector3) -> Vector3:
	global_point.y = cell_globlal_point.y
	var distance = global_point.distance_to(cell_globlal_point + Vector3.LEFT * CELL_SIZE_HALF)
	var dir = Vector3.LEFT
	var distance_aux = global_point.distance_to(cell_globlal_point + Vector3.RIGHT * CELL_SIZE_HALF)
	if distance_aux < distance:
		distance = distance_aux
		dir = Vector3.RIGHT
	distance_aux = global_point.distance_to(cell_globlal_point + Vector3.BACK * CELL_SIZE_HALF)
	if distance_aux < distance:
		distance = distance_aux
		dir = Vector3.BACK
	distance_aux = global_point.distance_to(cell_globlal_point + Vector3.FORWARD * CELL_SIZE_HALF)
	if distance_aux < distance:
		distance = distance_aux
		dir = Vector3.FORWARD
	return dir

func pathfiding_update() -> void:
	# restore connections of all the cells
	for id in CELL_COUNT:
		pathfinding_connect_points(id, id + 1)
		pathfinding_connect_points(id, id + GRAPH_SIZE)
		pathfinding_connect_points(id, id + GRAPH_SIZE + 1)
		pathfinding_connect_points(id, id + GRAPH_SIZE - 1)
	# call the method of all placeabes incharge of updating the pathfiding
	for p in _placeables:
		p.pathfiding_update()

func pathfinding_connect_points(id:int, to_id:int) -> void:
	if is_id_valid(id) and is_id_valid(to_id):
		if not _graph.are_points_connected(id, to_id):
			_graph.connect_points(id, to_id)

func pathfinding_disconnect_points(id:int, to_id:int) -> void:
	if is_id_valid(id) and is_id_valid(to_id):
		if _graph.are_points_connected(id, to_id):
			_graph.disconnect_points(id, to_id)

func _graph_generation():
	var start = OS.get_ticks_msec()
	
	OS.vsync_enabled = false
	
	for y in range(-GRAPH_SIZE_HALF, GRAPH_SIZE_HALF):
		for x in range(-GRAPH_SIZE_HALF , GRAPH_SIZE_HALF):
			var position = Vector3(x * CELL_SIZE + CELL_SIZE_HALF, 0, y * CELL_SIZE + CELL_SIZE_HALF)
#			var id = local_position_to_id(position)
			var id = _point_count
			_graph.add_point(id, position)
			# UP AND DOWN
			_graph_generation_connect_points(id, position + Vector3(0, 0, 1) * CELL_SIZE)
			_graph_generation_connect_points(id, position + Vector3(0, 0, -1) * CELL_SIZE)
			# RIGHT
			_graph_generation_connect_points(id, position + Vector3(1, 0, 0) * CELL_SIZE)
			if CONNECT_DIAGONALLY:
				_graph_generation_connect_points(id, position + Vector3(1, 0, 1) * CELL_SIZE)
				_graph_generation_connect_points(id, position + Vector3(1, 0, -1) * CELL_SIZE)
			# LEFT
			_graph_generation_connect_points(id, position + Vector3(-1, 0, 0) * CELL_SIZE)
			if CONNECT_DIAGONALLY:
				_graph_generation_connect_points(id, position + Vector3(-1, 0, 1) * CELL_SIZE)
				_graph_generation_connect_points(id, position + Vector3(-1, 0, -1) * CELL_SIZE)
			_cells.push_back(FDZGridCell.new())
			_point_count += 1
		$Label.text = "Adding points and connections %" + str(int(float(_point_count) / float(CELL_COUNT) * 100))
		yield()
	
	OS.vsync_enabled = true
	
	_dirty = true
	_generation_finished = true
	
	var total = (OS.get_ticks_msec() - start) / 1000.0
	$Label.text = "AStar generation ended" + "\n"
	$Label.text += "Duration: " + str(total) + " secs" + "\n"
	$Label.text += "Point Count: " + str(_point_count) + "/" + str(CELL_COUNT) + "\n"
	$Label.text += "_cells.size(): " + str(_cells.size()) + "\n"

func _graph_generation_connect_points(id:int, to_position:Vector3) -> void:
	var to_id = get_id_from_local_point(to_position)
	if id != to_id and _graph.has_point(to_id):
		_graph.connect_points(id, to_id)

func _draw_grid() -> void:
	_im = $Draw
	_im.clear()
	_im.begin(Mesh.PRIMITIVE_LINES)
	_im.set_color(Color.green)
	# horizontal
	var p1 = Vector3(-1, 0, 1) * GRAPH_SIZE_HALF * CELL_SIZE + IM_OFFSET
	var p2 = Vector3(-1, 0, -1) * GRAPH_SIZE_HALF * CELL_SIZE + IM_OFFSET
	# vertical
	var p3 = Vector3(1, 0, -1) * GRAPH_SIZE_HALF * CELL_SIZE + IM_OFFSET
	var p4 = Vector3(-1, 0, -1) * GRAPH_SIZE_HALF * CELL_SIZE + IM_OFFSET
	# add vertices
	for x in GRAPH_SIZE + 1:
		_im.add_vertex(p1)
		_im.add_vertex(p2)
		p1.x += CELL_SIZE
		p2.x += CELL_SIZE
		_im.add_vertex(p3)
		_im.add_vertex(p4)
		p3.z += CELL_SIZE
		p4.z += CELL_SIZE
	_im.end()

func _update_im() -> void:
	var POINTS = false
	var margin = CELL_SIZE_HALF - 0.1 
	if _im == null:
		_im = $Draw
#	_im.clear()
	_draw_grid()
	if POINTS:
		_im.begin(Mesh.PRIMITIVE_POINTS)
	else:
		_im.begin(Mesh.PRIMITIVE_LINES)
	_im.set_color(Color.red)
	for id in _point_count:
		var position = _graph.get_point_position(id)
		if _graph.is_point_disabled(id):
			if POINTS:
				_im.add_vertex(position + IM_OFFSET)
			else: # LINES
				# left
				_im.add_vertex(position + Vector3(-margin, 0, -margin) + IM_OFFSET)
				_im.add_vertex(position + Vector3(-margin, 0, margin) + IM_OFFSET)
				# right
				_im.add_vertex(position + Vector3(margin, 0, -margin) + IM_OFFSET)
				_im.add_vertex(position + Vector3(margin, 0, margin) + IM_OFFSET)
				# back
				_im.add_vertex(position + Vector3(-margin, 0, margin) + IM_OFFSET)
				_im.add_vertex(position + Vector3(margin, 0, margin) + IM_OFFSET)
				# forward
				_im.add_vertex(position + Vector3(-margin, 0, -margin) + IM_OFFSET)
				_im.add_vertex(position + Vector3(margin, 0, -margin) + IM_OFFSET)
	_im.end()
#	print("FDZGrid re-drawn")