# Main References

- RimWorld
- Age Of Empires II
- Diablo II
- PoE
- Banished
- Cities Skylines
- Kenshi

# Other References

- Foundation
- Dawn Of Men
- Judgment: Apocalypse Survival Simulation
- Stonehearth
- Castle Story

# Owned Games

- Breach & Clear
- Door Kickers
- RimWorld
- Survivalist

# God Games

- [30 Years of God Game History | Populous, Dungeon Keeper, Black & White, Spore and more](https://www.youtube.com/watch?v=gt4-tBFIcsI)
- Dungeon Keeper
- Black & White 1
- Rise to Ruins
- Tethered
- Townsmen VR
- Crest

# Real Time Tactics

- **Dustwind:** has auto attack
  - [DUSTWIND Review | The Fallout: Tactics Successor You Should Be Playing](https://www.youtube.com/watch?v=6nphbXe_uKg)

# Modern Military RTS

- *Source: https://www.reddit.com/r/RealTimeStrategy/comments/4xe0ij/modern_military_rts/*
- **Men of War: Assault Squad 2**
- **Call to arms** - *Men of War style gameplay with modern warfare*.
- **WARGAME** - *Large scale realistic RTS with Cold War to Modern era vehicles, units and weapons. Up to 10 player multiplayer and good campaign in all three games*.
- **Joint Task Force** - *Modern Military RTS with long and full campaign with storyline, plays out in Bosnia, Somalia, South America, Iraq, Afghanistan*.
- **Command & Conquer: Generals** - *Arcady RTS with "modern" units (most are futuristically upgraded)*.
- **Company of Heroes: Modern Combat** - *Plays like original Company of Heroes but with new maps, entirely new units and a brand new faction (China). It has updated unit stats and new doctrines as well*.
- **Act of Agression** - *Similar to Command & Conquer gameplay wise, modern real world units, same creators as WARGAME but more close in*.
- **World in Conflict** - *Call in units, world war three, cold war, Russian invasion on the USA*.