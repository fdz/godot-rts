class_name Utils

static func make_smooth_curve(toCurve:PoolVector3Array, smoothness:float = 1.0) -> PoolVector3Array:
#	toCurve = make_smooth_curve2(toCurve)
	# https://answers.unity.com/questions/392606/line-drawing-how-can-i-interpolate-between-points.html
	var points = null
	var curved_points = PoolVector3Array()
	var points_length = 0
	var curved_length = 0
	# this if could be removed for using less points than the original array,
	# useful for when the line has really close points,
	# that said it generates less accurate points but this could
#	if smoothness < 1.0:
#		smoothness = 1.0
	points_length = toCurve.size()
#	curved_length = (points_length * int(round(smoothness))) - 1
	curved_length = int(round(points_length * smoothness)) - 1
	var t = 0.0
	for point_int_time_on_curve in curved_length:
		t = inverse_lerp(0, curved_length, point_int_time_on_curve)
		points = PoolVector3Array(toCurve)
		var j = points_length - 1
		while j > 0:
			for i in j:
				points[i] = (1 - t) * points[i] + t * points[i + 1]
			j -= 1
		curved_points.append(points[0])
	curved_points.append(toCurve[toCurve.size() - 1])
	return curved_points

static func make_smooth_curve2(toCurve:PoolVector3Array, distance:float = 0.15) -> PoolVector3Array:
	var cur:Vector3
	var curve = PoolVector3Array()
	curve.push_back(toCurve[0])
	for i in toCurve.size() - 1:
		if i == 0:
			continue
		cur = toCurve[i]
		curve.push_back(cur.linear_interpolate(toCurve[i - 1], distance))
		curve.push_back(cur.linear_interpolate(toCurve[i + 1], distance))
	curve.push_back(toCurve[toCurve.size() - 1])
	return curve